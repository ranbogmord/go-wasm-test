module wasm-test1

go 1.21.1

require (
	github.com/google/uuid v1.3.1 // indirect
	github.com/klauspost/compress v1.10.3 // indirect
	github.com/maxence-charriere/go-app/v9 v9.8.0 // indirect
	nhooyr.io/websocket v1.8.7 // indirect
)
