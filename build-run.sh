#!/usr/bin/env bash

GOOS=js GOARCH=wasm go build -o ./web/app.wasm ./cmd/server
go run cmd/server/main.go

