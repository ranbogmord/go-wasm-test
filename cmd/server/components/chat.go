package components

import (
	"context"
	"fmt"
	"github.com/maxence-charriere/go-app/v9/pkg/app"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

type ChatMessage struct {
	Id      string
	Message string
}

type Chat struct {
	app.Compo
	connected    bool
	socket       *websocket.Conn
	errorMessage string
	messages     []ChatMessage
	newMessage   string
}

func (c *Chat) msgListener() {
	fmt.Println("Listening for messages")
	defer func() {
		c.socket.Close(websocket.StatusNormalClosure, "")
		c.connected = false
	}()
	for {
		var msg ChatMessage
		err := wsjson.Read(context.Background(), c.socket, &msg)
		if err != nil {
			c.errorMessage = err.Error()
			return
		}

		fmt.Printf("Got message: %v\n", msg)
		c.messages = append(c.messages, msg)
		c.Update()
	}
}

func (c *Chat) Render() app.UI {
	wsHost := app.Window().Get("wsHost").String()
	fmt.Println(wsHost)

	onConnect := app.EventHandler(func(ctx app.Context, e app.Event) {
		con, _, err := websocket.Dial(context.Background(), wsHost, nil)
		if err != nil {
			c.errorMessage = err.Error()
			return
		}
		c.socket = con
		go c.msgListener()

		c.connected = true
		c.errorMessage = ""
		c.messages = []ChatMessage{}
	})

	onDisconnect := app.EventHandler(func(ctx app.Context, e app.Event) {
		if c.socket != nil {
			c.socket.Close(websocket.StatusNormalClosure, "")
		}
		c.connected = false
	})

	sendMessage := app.EventHandler(func(ctx app.Context, e app.Event) {
		if c.socket == nil {
			c.errorMessage = "Not connected"
		}

		c.errorMessage = ""
		if len(c.newMessage) < 1 {
			return
		}

		err := wsjson.Write(context.Background(), c.socket, c.newMessage)
		if err != nil {
			c.errorMessage = err.Error()
			return
		}

		c.newMessage = ""
	})

	return app.Div().Body(
		app.H2().Text("Chat"),
		app.If(
			c.errorMessage != "",
			app.P().Textf("Something went wrong: %s", c.errorMessage),
		),
		app.If(
			c.connected,
			app.Div().Body(
				app.H3().Text("Connected to chat"),
				app.Button().Text("Disconnect").On("click", onDisconnect),
				app.Div().Body(
					app.Range(c.messages).Slice(func(i int) app.UI {
						return app.Div().Body(
							app.P().Textf("Msg from %s: %s", c.messages[i].Id, c.messages[i].Message),
						)
					}),
					app.Div().Body(
						app.Textarea().On("keyup", func(ctx app.Context, e app.Event) {
							c.newMessage = ctx.JSSrc().Get("value").String()
						}),
						app.Button().Text("Send").On("click", sendMessage),
					),
				),
			),
		).Else(
			app.H3().Text("Connect to chat"),
			app.Button().Text("Connect").On("click", onConnect),
		),
	)
}
