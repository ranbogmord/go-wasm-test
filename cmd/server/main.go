package main

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/maxence-charriere/go-app/v9/pkg/app"
	"log"
	"net/http"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
	"wasm-test1/cmd/server/components"
)

var sockets map[string]*websocket.Conn

func main() {
	app.Route("/", &components.Hello{})
	app.RunWhenOnBrowser()

	sockets = make(map[string]*websocket.Conn)

	wsfunc := http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		c, err := websocket.Accept(writer, request, nil)
		if err != nil {
			writer.WriteHeader(500)
			return
		}
		id := uuid.New().String()
		sockets[id] = c

		err = wsjson.Write(context.Background(), c, &components.ChatMessage{
			Id:      "system",
			Message: fmt.Sprintf("Client ID: %s", id),
		})
		if err != nil {
			return
		}

		go func() {
			ctx := context.Background()
			defer c.Close(websocket.StatusNormalClosure, "")

			for {
				var v string
				err = wsjson.Read(ctx, c, &v)
				if err != nil {
					c.Close(websocket.StatusInternalError, "failed to read")
					return
				}

				log.Printf("Got '%v' from the client", v)

				for _, client := range sockets {
					msg := &components.ChatMessage{
						Id:      id,
						Message: v,
					}
					err = wsjson.Write(context.Background(), client, msg)
					if err != nil {
						log.Printf("Failed to send message: %v\n", err)
					}
				}
			}
		}()
	})

	mux := http.NewServeMux()
	mux.Handle("/", &app.Handler{
		Name:        "hello",
		Description: "Hello GoApp",
		Title:       "Hello GoApp",
		RawHeaders: []string{
			"<script>window.wsHost = `ws://${window.location.host}/ws`</script>",
		},
	})
	mux.HandleFunc("/ws", wsfunc)

	log.Println("Server booted")
	log.Fatal(http.ListenAndServe("0.0.0.0:9000", mux))
}
