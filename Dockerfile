FROM golang:1.21

ADD ./ /app
WORKDIR /app

RUN ./build.sh
CMD ./main
