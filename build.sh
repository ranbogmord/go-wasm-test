#!/usr/bin/env bash

GOOS=js GOARCH=wasm go build -o ./web/app.wasm ./cmd/server
go build -o main cmd/server/main.go
GOOS=windows GOARCH=amd64 go build -o main cmd/server/main.go
